package com.hcloud.system.api.feign.user.factory;

import com.hcloud.system.api.feign.user.RemoteUserService;
import com.hcloud.system.api.feign.user.fallback.RemoteUserFallbackImpl;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @Auther hepangui
 * @Date 2018/11/8
 */
@Component
public class RemoteUserFallbackFactory implements FallbackFactory<RemoteUserService> {
    @Override
    public RemoteUserService create(Throwable throwable) {
        return new RemoteUserFallbackImpl(throwable);
    }
}
