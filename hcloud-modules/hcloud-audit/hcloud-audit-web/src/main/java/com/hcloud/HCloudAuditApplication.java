package com.hcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author hepg
 * @date 2018年10月26日
 * 审计中心，查看日志，以及各种操作记录，比如说财务充值之类
 */
@SpringBootApplication
public class HCloudAuditApplication {

    public static void main(String[] args) {
        SpringApplication.run(HCloudAuditApplication.class, args);
    }

}
