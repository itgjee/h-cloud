package com.hcloud.auth.api.token;

import com.hcloud.auth.api.config.LoginType;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * 三方登录token
 *
 * @author hepangui
 * @date 2018-12-13
 */
public class ThirdAuthenticationToken extends AbstractAuthenticationToken {

    private final Object principal;

    private LoginType loginType;

    public LoginType getLoginType() {
        return loginType;
    }

    public ThirdAuthenticationToken(String mobile, LoginType loginType) {
        super(null);
        this.principal = mobile;
        this.loginType = loginType;
        this.setAuthenticated(false);
    }

    public ThirdAuthenticationToken(Object principal, Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
        this.principal = principal;
        super.setAuthenticated(true);
    }


    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return this.principal;
    }

    @Override
    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
        if (isAuthenticated) {
            throw new IllegalArgumentException("Cannot set this token to trusted - use constructor which takes a GrantedAuthority list instead");
        } else {
            super.setAuthenticated(false);
        }
    }

    @Override
    public void eraseCredentials() {
        super.eraseCredentials();
    }

}
