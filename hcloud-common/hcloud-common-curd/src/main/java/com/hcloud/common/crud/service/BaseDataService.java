package com.hcloud.common.crud.service;

import com.hcloud.common.core.exception.ServiceException;
import com.hcloud.common.crud.entity.BaseEntity;
import com.hcloud.common.crud.repository.BaseRepository;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

public interface BaseDataService<Entity extends BaseEntity> {
    String POSTFIX_GT = "Gt";
    String POSTFIX_LT = "Lt";
    String POSTFIX_LIKEFONT = "LikeBefore";
    String POSTFIX_LIKEBACK = "LikeBack";
    String POSTFIX_LIKE = "Like";
    String POSTFIX_NOTEQUAL = "NotEqual";
    String POSTFIX_ISNULL = "IsNull";
    String POSTFIX_ISNOTNULL = "IsNotNull";
    String POSTFIX_GTEQ = "GtOrEqual";
    String POSTFIX_LTEQ = "LtOrEqual";

    /**
     * 添加一个实体，实体内应没有id，返回添加成功后的实体
     *
     * @param entity
     * @return
     */
    Entity add(Entity entity);

    /**
     * 批量添加一个实体集合，返回添加成功后的集合
     *
     * @param entities
     * @return
     */
    List<Entity> saveAll(List<Entity> entities);

    /**
     * 更新实体，必须保证id字段不为空
     *
     * @param entity
     * @return
     */
    Entity update(Entity entity);

    /**
     * 根据id获取实体，如果一级缓存中存在，则从以及缓存中取
     *
     * @param id
     * @return
     */
    Entity get(String id);

    /**
     * 删除一个实体，实体必须有id
     *
     * @param entity
     * @throws ServiceException
     */
    void delete(Entity entity);

    /**
     * 根据id删除实体
     *
     * @param id
     * @throws ServiceException
     */
    void deleteById(String id);

    boolean exists(String id);

    /**
     * 根据传入的example 进行查询，不分页，且条件都是equals
     *
     * @param example
     * @return
     */
    List<Entity> find(Example<Entity> example);

    /**
     * 分页查询，但是没有查询条件
     *
     * @param pageable
     * @return
     */
    Page<Entity> find(Pageable pageable);


    /**
     * 根据传入的Bean和sort查询对应的entity
     * 不进行分页，查出来的是全部
     *
     * @param entity
     * @param sort
     * @return
     */
    List<Entity> findByCondition(Entity entity, Sort sort);

    /**
     * 条件查询，根据bean中定义的字段名进行查询，
     * 具体字段命名方式参见 BaseDataServiceImpl
     *
     * @param entity 实际传入的应该是queryBean  继承自bean
     * @return
     */
    List<Entity> findByCondition(Entity entity);

    /**
     * 条件查询并分页
     *
     * @param pageable
     * @return
     */
    Page<Entity> findByCondition(Pageable pageable, Entity entity);


}