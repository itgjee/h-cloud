package com.hcloud.common.crud.repository;

import com.hcloud.common.crud.entity.BaseEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * 基础repository层
 * @Auther hepangui
 * @Date 2018/10/31
 */
@NoRepositoryBean
public interface BaseRepository<T extends BaseEntity> extends JpaRepository<T,String>, JpaSpecificationExecutor<T> {

}
