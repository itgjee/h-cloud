package com.hcloud.common.social.exception;

import org.springframework.security.core.AuthenticationException;

/**
 * @Auther hepangui
 * @Date 2018/12/13
 */
public class WrongQQCodeException extends AuthenticationException {
    public WrongQQCodeException(String msg) {
        super(msg);
    }
    public WrongQQCodeException(){
        this("qq登录信息错误");
    }
}
