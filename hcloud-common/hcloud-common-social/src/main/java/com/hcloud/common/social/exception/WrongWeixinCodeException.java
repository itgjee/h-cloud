package com.hcloud.common.social.exception;

import org.springframework.security.core.AuthenticationException;

/**
 * @Auther hepangui
 * @Date 2018/12/13
 */
public class WrongWeixinCodeException extends AuthenticationException {
    public WrongWeixinCodeException(String msg) {
        super(msg);
    }

    public WrongWeixinCodeException() {
        this("微信登录信息错误");
    }
}
